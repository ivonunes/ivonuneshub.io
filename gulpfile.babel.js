'use strict'

import gulp from 'gulp'
import del from 'del'
import runSequence from 'run-sequence'
import gulpLoadPlugins from 'gulp-load-plugins'
import { spawn } from "child_process"
import tildeImporter from 'node-sass-tilde-importer'
import fs from 'fs'
import clean from 'gulp-clean'

const $ = gulpLoadPlugins()
const browserSync = require('browser-sync').create()

const onError = (err) => {
    console.log(err)
}

let suppressHugoErrors = false;

// --

gulp.task('server', ['build'], () => {
    gulp.start('init-watch')
    $.watch(['archetypes/**/*', 'data/**/*', 'content/**/*', 'layouts/**/*', 'static/**/*', 'config.toml'], () => gulp.start('hugo'))
});

gulp.task('server:with-drafts', ['build-preview'], () => {
    gulp.start('init-watch')
    $.watch(['archetypes/**/*', 'data/**/*', 'content/**/*', 'layouts/**/*', 'static/**/*', 'config.toml'], () => gulp.start('hugo-preview'))
});

gulp.task('init-watch', () => {
    suppressHugoErrors = true;
    browserSync.init({
        server: {
            baseDir: 'public'
        },
        open: false
    })
    $.watch('src/sass/**/*.scss', () => gulp.start('sass'))
    $.watch('src/js/**/*.js', () => gulp.start('js-watch'))
    $.watch('src/img/**/*', () => gulp.start('img'))  
    $.watch('src/icons/**/*', () => gulp.start('icons'))  
})

gulp.task('build', () => {
    runSequence('pub-delete', ['sass', 'js', 'fonts', 'img', 'icons'], 'hugo', 'cleanup')
})

gulp.task('build-preview', () => {
    runSequence('pub-delete', ['sass', 'js', 'fonts', 'img', 'icons'], 'hugo-preview')
})


gulp.task('hugo', (cb) => {
    let baseUrl = process.env.URL;
    let args = baseUrl ? ['-b', baseUrl] : [];

    return spawn('hugo', args, { stdio: 'inherit' }).on('close', (code) => {
        if (suppressHugoErrors || code === 0) {
            browserSync.reload()
            cb()
        } else {
            console.log('hugo command failed.');
            cb('hugo command failed.');
        }
    })
})

gulp.task('cleanup', () => {
    gulp.src('public/features', {read: false})
        .pipe(clean());

    gulp.src('public/categories', {read: false})
        .pipe(clean());

    gulp.src('public/projects', {read: false})
        .pipe(clean());

    gulp.src('public/skills', {read: false})
        .pipe(clean());

    gulp.src('public/slides', {read: false})
        .pipe(clean());

    gulp.src('public/social', {read: false})
        .pipe(clean());

    gulp.src('public/tags', {read: false})
        .pipe(clean());
})

gulp.task('hugo-preview', (cb) => {
    let args = ['--buildDrafts', '--buildFuture'];
    if (process.env.DEPLOY_PRIME_URL) {
        args.push('-b')
        args.push(process.env.DEPLOY_PRIME_URL)
    }
    return spawn('hugo', args, { stdio: 'inherit' }).on('close', (code) => {
        if (suppressHugoErrors || code === 0) {
            browserSync.reload()
            cb()
        } else {
            console.log('hugo command failed.');
            cb('hugo command failed.');
        }
    })
})

// --

gulp.task('sass', () => {
    return gulp.src([
        'src/sass/**/*.scss'
    ])
    .pipe($.plumber({ errorHandler: onError }))
    .pipe($.print())
    .pipe($.sass({ precision: 5, importer: tildeImporter }))
    .pipe($.autoprefixer(['ie >= 10', 'last 2 versions']))
    .pipe($.if(true, $.cssnano({ discardUnused: false, minifyFontValues: false })))
    .pipe($.size({ gzip: true, showFiles: true }))
    .pipe(gulp.dest('static/css'))
    .pipe(browserSync.stream())
})

gulp.task('js-watch', ['js'], (cb) => {
    browserSync.reload();
    cb();
});

gulp.task('js', () => {
    return gulp.src([
        'src/js/**/*.js'
    ])
    .pipe($.plumber({ errorHandler: onError }))
    .pipe($.print())
    .pipe($.concat('app.js'))
    .pipe($.if(true, $.uglify()))
    .pipe($.size({ gzip: true, showFiles: true }))
    .pipe(gulp.dest('static/js'))
})

gulp.task('fonts', () => {
    return gulp.src('src/fonts/**/*.{woff,woff2}')
        .pipe(gulp.dest('static/fonts'));
});

gulp.task('img', () => {
    return gulp.src('src/img/**/*.{png,jpg,jpeg,gif,svg,webp,ico}')
        .pipe($.newer('static/img'))
        .pipe($.print())
        .pipe($.imagemin())
        .pipe(gulp.dest('static/img'));
});

gulp.task('icons', () => {
    return gulp.src('src/icons/**/*.{png,jpg,jpeg,gif,svg,webp,ico}')
        .pipe($.newer('static'))
        .pipe($.print())
        .pipe($.imagemin())
        .pipe(gulp.dest('static'));
});

gulp.task('pub-delete', () => {
    return del(['public/**', '!public', 'functions/**', '!functions'], {
      // dryRun: true,
      dot: true
    }).then(paths => {
      console.log('Files and folders deleted:\n', paths.join('\n'), '\nTotal Files Deleted: ' + paths.length + '\n');
    })
})
