+++
date = "2019-01-20T15:41:55+00:00"
image = "/img/2019-01-20-chippy-thumb.jpg"
order = 3
subtitle = "A simple <br>CHIP-8 emulator"
title = "Chippy"
type = "project"
link = "https://github.com/ivonunes/chippy"

+++
