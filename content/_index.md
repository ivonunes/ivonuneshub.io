---
type: page
title: Web Developer, Buxton & Manchester
location: "Buxton & <br>Manchester"
email: ivonunes@me.com
contact: Do you want to start a new project? Got any questions? Get in touch and I'll get back to you.
---

Hi, I'm Ivo. I'm a full-stack web developer currently based in Buxton. I work at [Digitl](https://www.digitl.agency), an ecommerce agency in Manchester and I take on freelance projects in my spare time.